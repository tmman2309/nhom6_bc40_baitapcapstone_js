export class Phone{
  constructor(name,type,price,quantity,img,screen,backCamera,frontCamera,desc){
    this.name = name;
    this.type = type;
    this.price = price;
    this.quantity = quantity;
    this.img = img;
    this.screen = screen;
    this.backCamera = backCamera;
    this.frontCamera = frontCamera;
    this.desc = desc;
  }
}