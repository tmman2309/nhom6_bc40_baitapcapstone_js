import { Phone } from "../models/phoneModel.js";
import {
  kiemTraBoTrong,
  kiemTraBoTrongType,
  kiemTraNumber,
  kiemTraSoLuong,
} from "./phoneValidate.js";
let BASE_URL = "https://63b53b1c0f49ecf5089e1b45.mockapi.io/phone";

// renderLayout
export let renderLayout = (phoneData) => {
  let contentLayout = "";

  phoneData.reverse().forEach((item) => {
    let contentHTML = /*html*/ `
      <tr>
        <td>${item.id}</td>
        <td>${item.type}</td>
        <td>${item.name}</td>
        <td>${item.price}</td>
        <td>${item.quantity}</td>
        <td>
          <img src="${item.img}" alt="" />
        </td>
        <td>
          <button onclick="xoaPhone('${item.id}')" class="btn btn-danger">Xóa</button>
          <button data-toggle="modal" data-target="#myModal" onclick="suaPhone('${item.id}')" class="btn btn-warning">Sửa</button>
        </td>
      </tr>
    `;
    contentLayout += contentHTML;
  });

  document.getElementById("tblDanhSachSP").innerHTML = contentLayout;
};

// layThongTinTuForm
export let layThongTinTuForm = () => {
  let name = document.getElementById("name").value;
  let type = document.getElementById("type").value;
  let price = document.getElementById("price").value;
  let quantity = document.getElementById("quantity").value;
  let img = document.getElementById("img").value;
  let screen = document.getElementById("screen").value;
  let backCamera = document.getElementById("backCamera").value;
  let frontCamera = document.getElementById("frontCamera").value;
  let desc = document.getElementById("desc").value;

  return new Phone(
    name,
    type,
    price,
    quantity,
    img,
    screen,
    backCamera,
    frontCamera,
    desc
  );
};

// showThongTinLenForm
export let showThongTinLenForm = (id) => {
  axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  })
    .then((res) => {
      // show thông tin lên form
      document.getElementById("name").value = res.data.name;
      document.getElementById("type").value = res.data.type;
      document.getElementById("price").value = res.data.price;
      document.getElementById("quantity").value = res.data.quantity;
      document.getElementById("img").value = res.data.img;
      document.getElementById("screen").value = res.data.screen;
      document.getElementById("backCamera").value = res.data.backCamera;
      document.getElementById("frontCamera").value = res.data.frontCamera;
      document.getElementById("desc").value = res.data.desc;

      // show button lên form
      document.getElementById("buttonArea").innerHTML = /*html*/ `
      <button
        id="capNhatPhoneButton"
        onclick="capNhatPhone('${res.data.id}')"
        class="btn btn-warning"
      >
        Cập Nhật
      </button>`;
    })
    .catch((err) => {
      console.log(err);
    });
};

// Bật & Tắt Loading
export let batLoading = () => {
  document.getElementById("spinner-loading").style.display = "flex";
};
export let tatLoading = () => {
  document.getElementById("spinner-loading").style.display = "none";
};

// reLoading
export let reLoading = () => {
  document.getElementById("name").value = "";
  document.getElementById("type").value = "1";
  document.getElementById("price").value = "";
  document.getElementById("quantity").value = "";
  document.getElementById("img").value = "";
  document.getElementById("screen").value = "";
  document.getElementById("backCamera").value = "";
  document.getElementById("frontCamera").value = "";
  document.getElementById("desc").value = "";

  document.getElementById("tbName").innerHTML = "";
  document.getElementById("tbType").innerHTML = "";
  document.getElementById("tbPrice").innerHTML = "";
  document.getElementById("tbQuantity").innerHTML = "";
  document.getElementById("tbImg").innerHTML = "";
  document.getElementById("tbScreen").innerHTML = "";
  document.getElementById("tbBackCamera").innerHTML = "";
  document.getElementById("tbFrontCamera").innerHTML = "";
  document.getElementById("tbDesc").innerHTML = "";

  document.getElementById("buttonArea").innerHTML = /*html*/ `
    <button
      id="themPhoneButton"
      onclick="themPhone()"
      class="btn btn-success"
    >
      Thêm
    </button>
  `;
};
window.reLoading = reLoading;

// onSuccess
export let onSuccess = (message) => {
  Toastify({
    text: message,
    duration: 3000,
    className: "info",
    gravity: "top",
    position: "right",
    offset: {
      x: 50,
      y: 10,
    },
    stopOnFocus: false,
    style: {
      background: "#fc5c7d",
    },
  }).showToast();
};

// hideModal
export let hideModal = () => {
  $(".modal-backdrop").remove();

  let bodyID = document.getElementById("bodyID");
  let modalID = document.getElementById("myModal");

  bodyID.style.removeProperty("padding-right");
  bodyID.classList.remove("modal-open");
  modalID.style.display = "none";
  modalID.style.removeProperty("padding-right");
};

// validate
export let validate = (phone) => {
  let isValid = true;

  let isValidName = kiemTraBoTrong(phone.name, "tbName");
  let isValidType = kiemTraBoTrongType(phone.type, "tbType");
  let isValidPrice =
    kiemTraBoTrong(phone.price, "tbPrice") &&
    kiemTraSoLuong(phone.price, "tbPrice", 1000, "*Giá phải từ 1.000đ") &&
    kiemTraNumber(phone.price, "tbPrice");
  let isValidQuantity =
    kiemTraBoTrong(phone.quantity, "tbQuantity") &&
    kiemTraSoLuong(phone.quantity, "tbQuantity", 1, "*Số lượng phải từ 1") &&
    kiemTraNumber(phone.quantity, "tbQuantity");
  let isValidImg = kiemTraBoTrong(phone.img, "tbImg");
  let isValidScreen = kiemTraBoTrong(phone.screen, "tbScreen");
  let isValidBackCamera = kiemTraBoTrong(phone.backCamera, "tbBackCamera");
  let isValidFrontCamera = kiemTraBoTrong(phone.frontCamera, "tbFrontCamera");
  let isValidDesc = kiemTraBoTrong(phone.desc, "tbDesc");

  isValid =
    isValidName &
    isValidType &
    isValidPrice &
    isValidQuantity &
    isValidImg &
    isValidScreen &
    isValidBackCamera &
    isValidFrontCamera &
    isValidDesc;

  return isValid;
};
