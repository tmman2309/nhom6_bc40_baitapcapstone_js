import {
  batLoading,
  hideModal,
  layThongTinTuForm,
  onSuccess,
  renderLayout,
  showThongTinLenForm,
  tatLoading,
  validate,
} from "./phoneController.js";

let BASE_URL = "https://63b53b1c0f49ecf5089e1b45.mockapi.io/phone";

// fetchPhoneList
let fetchPhoneList = () => {
  batLoading();
  axios({
    url: `${BASE_URL}`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      renderLayout(res.data);
    })
    .catch((err) => {
      tatLoading();
      console.log(err);
    });
};
fetchPhoneList();

// xoaPhone
let xoaPhone = (id) => {
  batLoading();

  axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  })
    .then((res) => {
      tatLoading();
      onSuccess("Xóa thành công!");
      fetchPhoneList();
    })
    .catch((err) => {
      tatLoading();
      console.log(err);
    });
};
window.xoaPhone = xoaPhone;

// themPhone
let themPhone = () => {
  let newPhone = layThongTinTuForm();
  let isValid = validate(newPhone);

  if (isValid) {
    batLoading();
    hideModal();

    axios({
      url: `${BASE_URL}`,
      method: "POST",
      data: newPhone,
    })
      .then((res) => {
        tatLoading();
        onSuccess("Thêm mới thành công!");
        fetchPhoneList();
      })
      .catch((err) => {
        tatLoading();
        console.log(err);
      });
  }
};
window.themPhone = themPhone;

// suaPhone
let suaPhone = (id) => {
  showThongTinLenForm(id);
};
window.suaPhone = suaPhone;

// capNhatPhone
let capNhatPhone = (id) => {
  let phoneUpdated = layThongTinTuForm();
  let isValid = validate(phoneUpdated);

  if (isValid) {
    batLoading();
    hideModal();

    axios({
      url: `${BASE_URL}/${id}`,
      method: "PUT",
      data: phoneUpdated,
    })
      .then((res) => {
        tatLoading();
        onSuccess("Cập nhật thành công!");
        fetchPhoneList();
      })
      .catch((err) => {
        tatLoading();
        console.log(err);
      });
  }
};
window.capNhatPhone = capNhatPhone;
