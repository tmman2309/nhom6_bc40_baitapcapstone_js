// Kiểm tra bỏ trống
export function kiemTraBoTrong(idString, idErr) {
  if (idString.length == 0) {
    document.getElementById(idErr).style.display = "block";
    document.getElementById(idErr).innerHTML = `*bắt buộc điền`;
    return false;
  } else {
    document.getElementById(idErr).style.display = "none";
    document.getElementById(idErr).innerHTML = "";
    return true;
  }
}

export function kiemTraBoTrongType(idString, idErr) {
  if (idString == "1") {
    document.getElementById(idErr).style.display = "block";
    document.getElementById(idErr).innerHTML = `*bắt buộc chọn`;
    return false;
  } else {
    document.getElementById(idErr).style.display = "none";
    document.getElementById(idErr).innerHTML = "";
    return true;
  }
}

// Kiểm tra số lượng kí tự
export function kiemTraSoLuong(idString, idErr, min, message) {
  if (idString < min) {
    document.getElementById(idErr).style.display = "block";
    document.getElementById(idErr).innerHTML = message;
    return false;
  } else {
    document.getElementById(idErr).style.display = "none";
    document.getElementById(idErr).innerHTML = "";
    return true;
  }
}

// Kiểm tra Regex
export function kiemTraNumber(idString, idErr) {
  var reg = /^\d+$/;
  if (reg.test(idString)) {
    document.getElementById(idErr).style.display = "none";
    document.getElementById(idErr).innerHTML = "";
    return true;
  } else {
    document.getElementById(idErr).style.display = "block";
    document.getElementById(idErr).innerHTML = `*Vui lòng nhập số!`;
    return false;
  }
}
