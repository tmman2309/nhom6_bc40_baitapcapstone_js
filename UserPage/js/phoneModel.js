export function Phone(
  _name,
  _price,
  _screen,
  _backCamera,
  _frontCamera,
  _img,
  _desc,
  _type,
  _id
) {
  this.name = _name;
  this.price = _price;
  this.screen = _screen;
  this.backCamera = _backCamera;
  this.frontCamera = _frontCamera;
  this.img = _img;
  this.desc = _desc;
  this.type = _type;
  this.id = _id;
  this.quantity = 1;
  this.totalPrice = () => {
    return this.price * this.quantity;
  };
}

export function PhoneMap(
  _name,
  _price,
  _screen,
  _backCamera,
  _frontCamera,
  _img,
  _desc,
  _type,
  _quantity,
  _id
) {
  this.name = _name;
  this.price = _price;
  this.screen = _screen;
  this.backCamera = _backCamera;
  this.frontCamera = _frontCamera;
  this.img = _img;
  this.desc = _desc;
  this.type = _type;
  this.id = _id;
  this.quantity = _quantity;
  this.totalPrice = () => {
    return this.price * this.quantity;
  };
}
