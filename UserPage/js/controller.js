// UX-UI
// loveClick
let loveClick = (id) => {
  let loveClick = document.querySelectorAll(".loveClick");
  let clickIndex = "";

  for (let i = 0; i < loveClick.length; i++) {
    if (i == id - 1) {
      clickIndex = loveClick[i];
      break;
    }
  }

  clickIndex.classList.toggle("love-click");
};
window.loveClick = loveClick;
// animationCart
let animateCart = () => {
  let cartID = document.getElementById("cart-haveproduct");
  let backDropID = document.getElementById("backdrop");

  cartID.classList.remove("shoppingcart-animation-close");
  cartID.classList.add("shoppingcart-animation");
  backDropID.classList.add("backdrop-animation");
  backDropID.classList.remove("backdrop-animation-close");
};
window.animateCart = animateCart;
let cartClose = () => {
  let cartID = document.getElementById("cart-haveproduct");
  let backDropID = document.getElementById("backdrop");

  cartID.classList.remove("shoppingcart-animation");
  cartID.classList.add("shoppingcart-animation-close");
  backDropID.classList.remove("backdrop-animation");
  backDropID.classList.add("backdrop-animation-close");
};
window.cartClose = cartClose;
// Bật & Tắt Loading
export let batLoading = () => {
  document.getElementById("spinner-loading").style.display = "flex";
};
export let tatLoading = () => {
  document.getElementById("spinner-loading").style.display = "none";
};
// Toastify
export let onSuccess = (message) => {
  Toastify({
    text: message,
    duration: 1500,
    className: "info",
    gravity: "bottom",
    position: "right",
    offset: {
      x: 50,
      y: 10,
    },
    stopOnFocus: false,
    style: {
      background: "#fc5c7d",
    },
  }).showToast();
};

// Func quản lý
// Func liên quan đến product
// push product từ API về mảng global
export let getProduct = () => {
  let productList = [];

  axios({
    url: `https://63c79df05c0760f69aba7ddf.mockapi.io/phone`,
    method: "GET",
  })
    .then((res) => {
      res.data.forEach((item) => {
        productList.push(item);
      });
    })
    .catch((err) => {
      console.log(err);
    });

  return productList;
};
// renderLayout Product
export let renderLayout = (productAPI) => {
  let contentProduct = "";

  productAPI.forEach((item) => {
    // check type
    let typeURL = "";
    if (item.type == "Iphone") {
      typeURL = "./image/iphone.png";
    } else if (item.type == "Samsung") {
      typeURL = "./image/samsung.png";
    } else {
      typeURL = "#";
    }

    // renderlayout (product)
    let contentHTML = /*html*/ `
        <div
        class="product-item bg-white rounded-3xl overflow-hidden justify-self-center mb-20"
      >
        <div class="stock-area flex justify-between px-6 pt-3 items-center">
          <img
            class="product-logo"
            src=${typeURL}
            alt="${item.name}"
          />
          <p class="product-instock italic text-lime-700 text-sm font-bold">
            In Stock
          </p>
        </div>
        <div class="item-img flex justify-center items-center py-8 px-6">
          <img
            src="${item.img}"
            alt="${item.name}"
          />
        </div>
        <div
          class="item-content text-white bg-neutral-700 px-6 py-3 text-sm rounded-3xl"
        >
          <h3 class="text-xl font-bold">${item.name}</h3>
          <hr />
          <p>Thương hiệu: ${item.type}</p>
          <p>Màn hình: ${item.screen}</p>
          <p>Camera trước: ${item.frontCamera}</p>
          <p>Camera sau: ${item.backCamera}</p>
          <hr />
          <div class="price-area flex justify-between items-center">
            <p class="font-bold text-lg">$${item.price}</p>
            <div class="price-area-right">
              <i
                onclick="loveClick('${item.id}')"
                class="fa fa-heart mr-3 hover:text-red-600 duration-200 cursor-pointer loveClick"
              ></i>
              <button onclick="themDienThoai('${item.id}')"
                class="bg-white hover:bg-lime-700 text-lime-700 font-bold hover:text-white py-2 px-4 border hover:border-transparent rounded duration-200"
              >
                Thêm
                <i class="fas fa-chevron-right text-xs"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
         `;
    contentProduct += contentHTML;
  });

  document.getElementById("productList").innerHTML = contentProduct;
};
// renderLayout Filter Product
export let renderLayoutFilter = (arrInArr) => {
  let contentProduct = "";
  arrInArr.forEach((element) => {
    let contentPerProduct = "";
    element.forEach((item) => {
      // check type
      let typeURL = "";
      if (item.type == "Iphone") {
        typeURL = "./image/iphone.png";
      } else if (item.type == "Samsung") {
        typeURL = "./image/samsung.png";
      } else {
        typeURL = "#";
      }

      // renderlayout (product)
      let contentHTML = /*html*/ `
        <div
        class="product-item bg-white rounded-3xl overflow-hidden w-72 justify-self-center mb-20"
      >
        <div class="stock-area flex justify-between px-6 pt-3 items-center">
          <img
            class="product-logo"
            src=${typeURL}
            alt="${item.name}"
          />
          <p class="product-instock italic text-lime-700 text-sm font-bold">
            In Stock
          </p>
        </div>
        <div class="item-img flex justify-center items-center py-8 px-6">
          <img
            src="${item.img}"
            alt="${item.name}"
          />
        </div>
        <div
          class="item-content text-white bg-neutral-700 px-6 py-3 text-sm rounded-3xl"
        >
          <h3 class="text-xl font-bold">${item.name}</h3>
          <hr />
          <p>Thương hiệu: ${item.type}</p>
          <p>Màn hình: ${item.screen}</p>
          <p>Camera trước: ${item.frontCamera}</p>
          <p>Camera sau: ${item.backCamera}</p>
          <hr />
          <div class="price-area flex justify-between items-center">
            <p class="font-bold text-lg">$${item.price}</p>
            <div class="price-area-right">
              <i
                onclick="loveClick('${item.id}')"
                class="fa fa-heart mr-3 hover:text-red-600 duration-200 cursor-pointer loveClick"
              ></i>
              <button onclick="themDienThoai('${item.id}')"
                class="bg-white hover:bg-lime-700 text-lime-700 font-bold hover:text-white py-2 px-4 border hover:border-transparent rounded duration-200"
              >
                Thêm
                <i class="fas fa-chevron-right text-xs"></i>
              </button>
            </div>
          </div>
        </div>
      </div>
         `;
      contentPerProduct += contentHTML;
    });
    contentProduct += contentPerProduct;
  });

  document.getElementById("productList").innerHTML = contentProduct;
};
export let checkAndShowBrand = (
  brandList,
  brandID,
  productListRender,
  productList
) => {
  if (document.getElementById(brandID).checked) {
    productListRender.push(brandList);
    renderLayoutFilter(productListRender);
  } else {
    let index = productListRender.findIndex((ele) => ele == brandList);
    if (index != -1) {
      productListRender.splice(index, 1);
    }
    renderLayoutFilter(productListRender);
  }

  if (productListRender.length == 0) {
    renderLayout(productList);
  }
};

// Func liên quan đến cart
// renderLayoutCart
export let renderLayoutCart = (cartItemList) => {
  // product
  let contentProduct = "";

  cartItemList.forEach((item) => {
    let contentHTML = /*html*/ `
      <div class="flex items-center hover:bg-gray-100 -mx-8 px-6 py-5">
      <div class="flex w-2/5">
        <div class="w-20">
          <img class="h-24" src="${item.img}" alt="" />
        </div>
        <div class="flex flex-col justify-between ml-4 flex-grow">
          <span class="font-bold text-sm">${item.name}</span>
          <span class="text-indigo-600 text-xs">${item.type}</span>
          <span>
            <button onclick="removeItem('${
              item.id
            }')" class="font-semibold hover:text-red-600 text-red-500 text-xs">Remove</button>
          </span>
        </div>
      </div>
      <div class="flex justify-center w-1/5">
        <button onclick ="removeQuantity('${item.id}')">
        <svg
          class="fill-current text-gray-600 w-3"
          viewBox="0 0 448 512"
        >
          <path
            d="M416 208H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h384c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"
          />
        </svg>
        </button>

        <input
          class="quantity-item mx-2 border text-center w-12"
          type="text"
          value="${item.quantity}"
          disabled
        />
        <button onclick ="addQuantity('${item.id}')">
        <svg
          class="fill-current text-gray-600 w-3"
          viewBox="0 0 448 512"
        >
          <path
            d="M416 208H272V64c0-17.67-14.33-32-32-32h-32c-17.67 0-32 14.33-32 32v144H32c-17.67 0-32 14.33-32 32v32c0 17.67 14.33 32 32 32h144v144c0 17.67 14.33 32 32 32h32c17.67 0 32-14.33 32-32V304h144c17.67 0 32-14.33 32-32v-32c0-17.67-14.33-32-32-32z"
          />
        </svg>
        </button>
      </div>
      <span class="text-center w-1/5 font-semibold text-sm"
        >$${item.price}.00</span
      >
      <span class="text-center w-1/5 font-semibold text-sm"
        >$${item.totalPrice()}.00</span
      >
    </div>
      `;
    contentProduct += contentHTML;
  });

  document.getElementById("productlist-detail").innerHTML = contentProduct;

  // Quantity title
  let quantity = quantityTotal();
  let contentTitle = /*html*/ `
    <h1 class="font-semibold text-2xl">Shopping Cart</h1>
    <h2 class="font-semibold text-2xl">${quantity} Items</h2>
  `;

  document.getElementById("cartTitle").innerHTML = contentTitle;

  // Quantity icon
  let contentQuantity = /*html*/ `
  <span class="absolute text-xs text-center rounded-full">${quantity}</span>
 `;

  document.getElementById("cartIconQuantity").innerHTML = contentQuantity;
};
export let renderLayoutCart0 = () => {
  document.getElementById("productlist-detail").innerHTML = /*html*/ `
    <div class="flex h-full justify-center items-center">
      <p class="font-semibold text-gray-600 text-base uppercase text-center">"Look Like You Haven't Added Any Product Into The Cart"</p>
    </div>
   `;
};
// renderLayoutCartCheckout
export let renderLayoutCartCheckout = (cartItemList) => {
  // detail
  let contentCheckoutDetail = "";

  cartItemList.forEach((item) => {
    let contentHTML = /*html*/ `
      <div class="flex justify-between pl-3 mb-3">
        <span class="font-normal text-xs">${item.quantity} x ${item.name}</span>
        <span class="font-normal text-xs">$${item.totalPrice()}.00</span>
      </div>
     `;
    contentCheckoutDetail += contentHTML;
  });

  document.getElementById("checkout-detail").innerHTML = contentCheckoutDetail;

  // total
  let totalPrice = 0;

  cartItemList.forEach((item) => {
    let totalPerItem = item.quantity * item.price;
    totalPrice += totalPerItem;
  });

  let shippingValue = document.getElementById("shipping").value;

  if (shippingValue == "standard") {
    totalPrice += 10;
  } else if (shippingValue == "fast") {
    totalPrice += 20;
  }

  let contentCheckoutTotal = /*html*/ `
    <span>Total cost</span>
    <span>$${totalPrice}.00</span>
   `;

  document.getElementById("checkout-total").innerHTML = contentCheckoutTotal;

  return totalPrice;
};
window.renderLayoutCartCheckout = renderLayoutCartCheckout;
// renderLayoutCartConfỉm
export let renderLayoutCartConfirm = (cartItemList) => {
  let totalPrice = renderLayoutCartCheckout(cartItemList);
  let randomNumber = Math.floor(Math.random() * 1001);
  let contentConfirm = /*html*/ `
    <p class="font-normal text-sm capitalize mt-6"><span class="font-semibold">Your Order ID:</span> ${randomNumber}.</p>
    <p class="font-normal text-sm capitalize mt-4"><span class="font-semibold">Payment:</span> $${totalPrice}.00 (Cash on Delivery).</p>
    <p class="font-normal text-sm capitalize mt-4 mb-6"><span class="font-semibold">Shipment:</span> The products will be delivered to you in 3-5 working days.</p>
   `;

  document.getElementById("confirm-detail").innerHTML = contentConfirm;
};
// quantityTotal
let quantityTotal = () => {
  let quantityList = document.querySelectorAll(".quantity-item");
  let quantityTotal = 0;

  for (let i = 0; i < quantityList.length; i++) {
    let quantityValue = quantityList[i].value * 1;

    quantityTotal += quantityValue;
  }

  return quantityTotal;
};
