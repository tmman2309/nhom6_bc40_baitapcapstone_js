import {
  batLoading,
  checkAndShowBrand,
  getProduct,
  onSuccess,
  renderLayout,
  renderLayoutCart,
  renderLayoutCart0,
  renderLayoutCartCheckout,
  renderLayoutCartConfirm,
  tatLoading,
} from "./controller.js";
import { Phone, PhoneMap } from "./phoneModel.js";

// Func Quản Lý
const BASE_URL = "https://63c79df05c0760f69aba7ddf.mockapi.io/phone";

// getProduct
let productList = getProduct();

// filterbrand
let filterBrand = () => {
  batLoading();
  let productList = [];
  let productListRender = [];
  let iphoneList = [];
  let samsungList = [];

  axios({
    url: `${BASE_URL}`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      res.data.forEach((item) => {
        productList.push(item);
      });

      // filter
      productList.forEach((element) => {
        if (element.type == "Iphone") {
          iphoneList.push(element);
        } else if (element.type == "Samsung") {
          samsungList.push(element);
        }
      });

      // show layout
      checkAndShowBrand(iphoneList, "iphone", productListRender, productList);
      checkAndShowBrand(samsungList, "samsung", productListRender, productList);
    })
    .catch((err) => {
      tatLoading();
      console.log(err);
    });
  {
  }
};
window.filterBrand = filterBrand;

// fetchPhoneList
let fetchPhoneList = () => {
  batLoading();
  axios({
    url: `${BASE_URL}`,
    method: "GET",
  })
    .then((res) => {
      tatLoading();
      renderLayout(res.data);
    })
    .catch((err) => {
      tatLoading();
      console.log(err);
    });
};
fetchPhoneList();

// themDienThoai vào Cart
let cartItemList = [];

// get data từ localstorage

let cartItemListJson = localStorage.getItem("cart");

if (cartItemListJson != null) {
  let cartItemListNoFunc = JSON.parse(cartItemListJson);

  cartItemList = cartItemListNoFunc.map((item) => {
    return new PhoneMap(
      item.name,
      item.price,
      item.screen,
      item.backCamera,
      item.frontCamera,
      item.img,
      item.desc,
      item.type,
      item.quantity,
      item.id
    );
  });
  renderLayoutCart(cartItemList);
} else {
  renderLayoutCart(cartItemList);
  renderLayoutCart0();
}
if (cartItemList.length == 0) {
  renderLayoutCart0();
}

let themDienThoai = (id) => {
  // thêm điện thoại vào giỏ và check duplicate
  productList.forEach((item) => {
    if (id == item.id) {
      let cartItem = new Phone(
        item.name,
        item.price,
        item.screen,
        item.backCamera,
        item.frontCamera,
        item.img,
        item.desc,
        item.type,
        item.id
      );
      if (cartItemList.length == 0) {
        cartItemList.push(cartItem);
      } else {
        // check Dupplicate
        let index = cartItemList.findIndex((ele) => ele.id == cartItem.id);
        if (index == -1) {
          cartItemList.unshift(cartItem);
        } else {
          cartItemList[index].quantity++;

          let middleman = cartItemList[index];
          cartItemList.splice(index, 1);
          cartItemList.unshift(middleman);
        }
      }
    }
  });

  // toastify
  onSuccess("Đã thêm vào giỏ hàng!");

  // convert & lưu data json vào localstorage
  let cartItemListJson = JSON.stringify(cartItemList);
  localStorage.setItem("cart", cartItemListJson);

  // renderLayoutCart
  renderLayoutCart(cartItemList);
};
window.themDienThoai = themDienThoai;

// add & remove quantity
let addQuantity = (id) => {
  let index = cartItemList.findIndex((item) => item.id == id);

  cartItemList[index].quantity++;

  let cartItemListJson = JSON.stringify(cartItemList);
  localStorage.setItem("cart", cartItemListJson);

  renderLayoutCart(cartItemList);
};
window.addQuantity = addQuantity;
let removeQuantity = (id) => {
  let index = cartItemList.findIndex((item) => item.id == id);

  if (cartItemList.length > 0 && cartItemList[index].quantity == 1) {
    cartItemList.splice(index, 1);
  } else {
    cartItemList[index].quantity--;
  }

  let cartItemListJson = JSON.stringify(cartItemList);
  localStorage.setItem("cart", cartItemListJson);

  renderLayoutCart(cartItemList);

  if (cartItemList.length == 0) {
    renderLayoutCart0();
  }
};
window.removeQuantity = removeQuantity;
let removeItem = (id) => {
  let index = cartItemList.findIndex((item) => item.id == id);

  if (cartItemList.length > 0) {
    cartItemList.splice(index, 1);
    renderLayoutCart(cartItemList);
  }

  let cartItemListJson = JSON.stringify(cartItemList);
  localStorage.setItem("cart", cartItemListJson);

  if (cartItemList.length == 0) {
    renderLayoutCart0();
  }
};
window.removeItem = removeItem;
let removeItemAll = () => {
  cartItemList = [];

  let cartItemListJson = JSON.stringify(cartItemList);
  localStorage.setItem("cart", cartItemListJson);

  renderLayoutCart(cartItemList);
  renderLayoutCart0();
};
window.removeItemAll = removeItemAll;

// Checkout Cart
let checkoutOpen = () => {
  let cartID = document.getElementById("cart-haveproduct");
  let checkoutID = document.getElementById("checkout-cart");

  if (cartItemList.length != 0) {
    cartID.classList.add("shoppingcart-animation-close");
    cartID.classList.remove("shoppingcart-animation");
    checkoutID.classList.remove("checkout-animation-close");
    checkoutID.classList.add("checkout-animation");
  }

  renderLayoutCartCheckout(cartItemList);
};
window.checkoutOpen = checkoutOpen;
let checkoutClose = () => {
  let checkoutID = document.getElementById("checkout-cart");
  let backDropID = document.getElementById("backdrop");

  checkoutID.classList.add("checkout-animation-close");
  checkoutID.classList.remove("checkout-animation");
  backDropID.classList.add("backdrop-animation-close");
  backDropID.classList.remove("backdrop-animation");
};
window.checkoutClose = checkoutClose;
let confirmClose = () => {
  let confirmID = document.getElementById("confirm-cart");
  let backDropID = document.getElementById("backdrop");

  confirmID.classList.add("checkout-animation-close");
  confirmID.classList.remove("confirm-animation");
  backDropID.classList.add("backdrop-animation-close");
  backDropID.classList.remove("backdrop-animation");

  fetchPhoneList();
};
window.confirmClose = confirmClose;
let confirmOpen = () => {
  let confirmID = document.getElementById("confirm-cart");
  let checkoutID = document.getElementById("checkout-cart");

  checkoutID.classList.add("checkout-animation-close");
  checkoutID.classList.remove("checkout-animation");
  confirmID.classList.remove("confirm-animation-close");
  confirmID.classList.add("confirm-animation");

  renderLayoutCartConfirm(cartItemList);
  removeItemAll();
};
window.confirmOpen = confirmOpen;

// Shipment
let shipCheck = () => {
  renderLayoutCartCheckout(cartItemList);
};
window.shipCheck = shipCheck;
